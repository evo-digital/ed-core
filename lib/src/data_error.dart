abstract class DataError<D, E> {
  const DataError._internal(this.data, this.error, this.success);

  const DataError.success(D data) : this._internal(data, null, true);

  const DataError.error(E error) : this._internal(null, error, false);

  final D? data;
  final E? error;
  final bool success;

  void when({
    required void Function(D? data) onData,
    required void Function(E? error) onError,
  }) {
    if (success) {
      onData(data);
    } else {
      onError(error);
    }
  }
}
